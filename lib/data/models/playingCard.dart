import 'package:flutter/cupertino.dart';

class PlayingCard {

  int id;
  String name;
  String collection;
  String rarity;
  String image;
  String guest;
  String artist;
  String document;

  PlayingCard(this.id, this.name, this.collection, this.rarity, this.image, this.guest, this.artist, this.document);

  factory PlayingCard.fromJson(Map<String, dynamic> json, String document) {
    return PlayingCard(
      json['id'] as int,
      json['name'] as String,
      json['collection'] as String,
      json['rarity'] as String,
      json['image'] as String,
      json['guest'] as String,
      json['artist'] as String,
      document
    );
  }

}