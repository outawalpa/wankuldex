class Artist {

  String name;

  Artist(this.name);

  factory Artist.fromJson(Map<String, dynamic> json) {
    return Artist(
      json['name'] as String,
    );
  }

}