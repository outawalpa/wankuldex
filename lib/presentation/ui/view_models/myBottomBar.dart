import 'package:circular_bottom_navigation/circular_bottom_navigation.dart';
import 'package:circular_bottom_navigation/tab_item.dart';
import 'package:flutter/material.dart';
import 'package:wankuldex/presentation/ui/views/collectionPage.dart';
import 'package:wankuldex/presentation/ui/views/homePage.dart';
import 'package:wankuldex/presentation/ui/views/settingsPage.dart';

List<TabItem> tabItems = List.of([
  TabItem(Icons.home, "Accueil", Colors.black, labelStyle: const TextStyle(fontWeight: FontWeight.normal)),
  TabItem(Icons.collections, "Collection", Colors.black, labelStyle: const TextStyle(fontWeight: FontWeight.normal)),
  TabItem(Icons.rule, "Règles", Colors.black, labelStyle: const TextStyle(fontWeight: FontWeight.normal)),
]);

CircularBottomNavigation myBottomBar(BuildContext context, int indexSelected) {

  CircularBottomNavigationController navigationController = CircularBottomNavigationController(indexSelected);

  return CircularBottomNavigation(
    tabItems,
    barBackgroundColor: Colors.black,
    circleSize: 50,
    circleStrokeWidth: 2,
    iconsSize: 25,
    controller: navigationController,
    selectedCallback: (int? index) {
      if (index == 0 && indexSelected != 0) {
        Navigator.pushReplacement(
            context,
            MaterialPageRoute(builder: (context) => const HomePage())
        );
      } else if (index == 1 && indexSelected != 1) {
        Navigator.pushReplacement(
            context,
            MaterialPageRoute(builder: (context) => const CollectionPage())
        );
      } else if (index == 2 && indexSelected != 2){
        Navigator.pushReplacement(
            context,
            MaterialPageRoute(builder: (context) => const SettingsPage())
        );
      }
    },
  );
}