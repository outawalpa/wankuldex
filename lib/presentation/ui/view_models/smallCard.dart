import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:wankuldex/data/models/playingCard.dart';
import 'package:wankuldex/presentation/ui/views/cardPage.dart';

Container smallCard(BuildContext context, PlayingCard card) {
  return Container(
    margin: const EdgeInsets.all(10),
    child: InkWell(
      onTap: (){
        showCupertinoModalPopup(
            context: context,
            builder: (context) => CardPage(card: card)
        );
      },
      child: Image.network(card.image),
    )
  );
}