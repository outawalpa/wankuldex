import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:wankuldex/data/models/playingCard.dart';
import 'package:wankuldex/presentation/ui/view_models/myBottomBar.dart';
import 'package:wankuldex/presentation/ui/view_models/smallCard.dart';

class CollectionPage extends StatefulWidget {
  const CollectionPage({super.key});

  @override
  State<CollectionPage> createState() => _CollectionPageState();
}

class _CollectionPageState extends State<CollectionPage> {

  Stream<QuerySnapshot> _cardsStream = FirebaseFirestore.instance
      .collection('cards')
      .orderBy('id')
      .snapshots();

  @override
  void initState() {
    _cardsStream = FirebaseFirestore.instance
        .collection('cards')
        .orderBy('id')
        .snapshots();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [

            StreamBuilder<QuerySnapshot>(
                stream: _cardsStream,
                builder: (context, snapshot) {
                  if (!snapshot.hasData) return const CircularProgressIndicator(color: Colors.black,);
                  return Expanded(
                      child: GridView.builder(
                          gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                              crossAxisCount: crossAxisCount(),
                              childAspectRatio: 3/4,
                              crossAxisSpacing: 0,
                              mainAxisSpacing: 0),
                          itemCount: snapshot.data!.size,
                          itemBuilder: (context, index) {
                            PlayingCard card = PlayingCard.fromJson(snapshot.data!.docs[index].data() as Map<String, dynamic>, snapshot.data!.docs[index].id);
                            return smallCard(context, card);
                          })
                  );
                })
          ],
        ),
      ),
      bottomNavigationBar: myBottomBar(context, 1),
    );
  }

  int crossAxisCount() {

    int result = 1;
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;
    Orientation orientation = MediaQuery.of(context).orientation;

    if (orientation == Orientation.portrait) { //Portrait

      result = (width/120).round();

    } else { //Landscape

      result = (height/75).floor();

    }

    return result;

  }
}