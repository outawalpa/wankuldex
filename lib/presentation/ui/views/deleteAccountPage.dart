import 'dart:ui';
import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:wankuldex/presentation/ui/views/connectionPage.dart';
import 'package:wankuldex/presentation/ui/views/settingsPage.dart';

class DeleteAccountPage extends StatefulWidget {

  const DeleteAccountPage({super.key});

  @override
  State<DeleteAccountPage> createState() => _DeleteAccountPageState();
}

class _DeleteAccountPageState extends State<DeleteAccountPage> with TickerProviderStateMixin {

  late AnimationController controllerIndicator;

  final _formKey = GlobalKey<FormState>();

  final usernameController = TextEditingController();
  final passwordController = TextEditingController();

  bool connecting = false;

  @override
  void initState() {
    super.initState();

    // Initialize the spinner
    controllerIndicator = AnimationController(
      vsync: this,
      duration: const Duration(seconds: 2),
    )..addListener(() {
      setState(() {});
    });
    controllerIndicator.repeat(reverse: true);

  }

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      appBar: AppBar(
        title: const Text("Wankuldex"),
      ),
      backgroundColor: Colors.transparent,
      body: BackdropFilter(
        filter: ImageFilter.blur(sigmaX: 10, sigmaY: 10),
        child: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.all(20),
            child: Center(
              child: Form(
                key: _formKey,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    const Text(
                      'Une reconnexion est nécessaire pour suprimmer le compte.',
                      style: TextStyle(
                          fontSize: 25,
                          fontWeight: FontWeight.normal
                      ),
                    ),
                    const SizedBox(height: 10),
                    const Text(
                      'Email',
                      style: TextStyle(
                          fontSize: 25,
                          fontWeight: FontWeight.bold
                      ),
                    ),
                    TextFormField(
                      cursorColor: Colors.black,
                      decoration: const InputDecoration(
                        fillColor: Colors.white,
                        filled: true,
                        border: OutlineInputBorder(
                            borderSide: BorderSide(
                              color: Colors.black,
                            )),
                        focusColor: Colors.white,
                        focusedBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                              color: Colors.grey,
                            )),
                        hintText: 'Email',
                      ),
                      controller: usernameController,
                      keyboardType: TextInputType.emailAddress,
                      validator: (value) {
                        if (value == null || value.isEmpty) {
                          return 'Email requis';
                        }
                        return null;
                      },
                    ),
                    const SizedBox(height: 10),
                    const Text(
                      'Mot de passe',
                      style: TextStyle(
                          fontSize: 25,
                          fontWeight: FontWeight.bold
                      ),
                    ),
                    TextFormField(
                      cursorColor: Colors.black,
                      decoration: const InputDecoration(
                        fillColor: Colors.white,
                        filled: true,
                        border: OutlineInputBorder(
                            borderSide: BorderSide(
                              color: Colors.black,
                            )),
                        focusColor: Colors.white,
                        focusedBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                              color: Colors.grey,
                            )),
                        hintText: 'Mot de passe',
                      ),
                      controller: passwordController,
                      keyboardType: TextInputType.visiblePassword,
                      obscureText: true,
                      validator: (value) {
                        if (value == null || value.isEmpty) {
                          return 'Mot de passe requis';
                        }
                        return null;
                      },
                    ),

                    const SizedBox(height: 30),

                    Visibility(
                        visible: connecting,
                        child: Container(
                          alignment: Alignment.center,
                          child: SizedBox(
                              width: 50,
                              height: 50,
                              child: CircularProgressIndicator(
                                value: controllerIndicator.value,
                                semanticsLabel: 'Linear progress indicator',
                                color: Colors.black,
                              )
                          ),
                        )
                    ),

                    Visibility(
                      visible: !connecting,
                      child: ElevatedButton(
                        style: ElevatedButton.styleFrom(backgroundColor: Colors.black),
                        child: const Text("CONNEXION"),
                        onPressed: () {
                          signIn(context);
                        },
                      ),
                    ),

                    Visibility(
                      visible: !connecting,
                      child: ElevatedButton(
                        style: ElevatedButton.styleFrom(backgroundColor: Colors.red),
                        child: const Text("ANNULER"),
                        onPressed: () {

                          controllerIndicator.dispose();

                          Navigator.pushReplacement(
                              context,
                              MaterialPageRoute(builder: (context) => const SettingsPage())
                          );
                        },
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }

  signIn(BuildContext context) async {
    if (_formKey.currentState!.validate()) {

      connecting = true;

      await FirebaseAuth.instance.signInWithEmailAndPassword(
          email: usernameController.text,
          password: passwordController.text)
          .then((userCredential) async {
            
            String uid = userCredential.user!.uid;

            await FirebaseAuth.instance.currentUser!.delete()
                .then((value) {

                  FirebaseFirestore.instance
                    .collection('users')
                    .doc(uid)
                    .delete()
                    .then((value) => debugPrint('5555555555'))
                    .onError((error, stackTrace) => debugPrint('999999999 $error'));

                  controllerIndicator.dispose();

                  Navigator.pushReplacement(
                      context,
                      MaterialPageRoute(builder: (context) => const ConnectionPage())
                  );
                })
                .catchError((e) {
                  e as FirebaseAuthException;
                  
                  debugPrint('ERROR : $e');

                  AwesomeDialog(
                    context: context,
                    dialogType: DialogType.error,
                    animType: AnimType.rightSlide,
                    title: 'Erreur',
                    desc: "Une erreur s'est produite. code ${e.code} ",
                    btnOkOnPress: () {},
                  ).show();

                  usernameController.clear();
                  passwordController.clear();
                  connecting = false;
                });

            controllerIndicator.dispose();

          })
          .catchError((e) {

            e as FirebaseAuthException;
            debugPrint('ERROR : $e');

            AwesomeDialog(
              context: context,
              dialogType: DialogType.error,
              animType: AnimType.rightSlide,
              title: 'Erreur',
              desc: "Une erreur s'est produite. code ${e.code} ",
              btnOkOnPress: () {},
            ).show();

            usernameController.clear();
            passwordController.clear();
            connecting = false;

          });

    }
  }
}