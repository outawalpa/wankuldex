import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';

import 'package:wankuldex/presentation/ui/view_models/myBottomBar.dart';

class HomePage extends StatefulWidget {
  const HomePage({super.key});

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {

  List<Map<String, String>> items = [
    {
      'image': 'images/carousel_image_1.png',
      'text': 'wankul.fr',
      'url': 'https://wankul.fr',
    },
    {
      'image': 'images/carousel_image_2.png',
      'text': 'La collection Origins enfin disponible !',
      'url': 'https://wankul.fr/collections/cartes',
    }
  ];

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      body: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Image.asset('images/wankil-studio.jpg'),
          Container(
            padding: const EdgeInsets.all(25),
            decoration: const BoxDecoration(
              color: Colors.black,
            ),
            child: Image.network('https://wankul.fr/cdn/shop/files/Wankul_Logo_Blanc.png?v=1669131313&width=500'),
          ),
          Expanded(
              child: Center(
                child: CarouselSlider(
                  options: CarouselOptions(
                    //height: 200,
                    aspectRatio: 16/9,
                    viewportFraction: 0.7,
                    initialPage: 0,
                    enableInfiniteScroll: true,
                    reverse: false,
                    autoPlay: true,
                    autoPlayInterval: const Duration(seconds: 5),
                    autoPlayAnimationDuration: const Duration(milliseconds: 800),
                    autoPlayCurve: Curves.fastOutSlowIn,
                    enlargeCenterPage: true,
                    enlargeFactor: 0.3,
                    scrollDirection: Axis.horizontal,
                  ),
                  items: items.map((i) {
                    return Builder(
                      builder: (BuildContext context) {
                        return Container(
                            //width: MediaQuery.of(context).size.width,
                            margin: const EdgeInsets.symmetric(horizontal: 5.0),
                            decoration: BoxDecoration(
                                //color: Colors.white
                              border: Border.all(color: Colors.black)
                            ),
                            child: InkWell(
                              onTap: () => _launchUrl(i['url']!),
                              child: Stack(
                                fit: StackFit.expand,
                                children: [
                                  Opacity(opacity: 0.4,
                                    child: Image.asset(i['image']!, fit: BoxFit.cover),
                                  ),
                                  Center(
                                    child: Text(i['text']!,
                                      textAlign: TextAlign.center,
                                      style: const TextStyle(
                                        fontSize: 50,
                                      ),
                                    ),
                                  )
                                ],
                              ),
                            )
                        );
                      },
                    );
                  }).toList(),
                )
              )
          )
        ],
      ),
      bottomNavigationBar: myBottomBar(context, 0),
    );
  }

  Future<void> _launchUrl(String url) async {
    Uri uri = Uri.parse(url);
    if (!await launchUrl(uri)) {
      throw Exception('Could not launch $uri');
    }
  }
}