import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:wankuldex/presentation/ui/view_models/myBottomBar.dart';

class InformationPage extends StatefulWidget {
  const InformationPage({super.key});

  @override
  State<InformationPage> createState() => _InformationPageState();
}

class _InformationPageState extends State<InformationPage> {

  final Stream<QuerySnapshot> _collectionStream = FirebaseFirestore.instance
      .collection('collection')
      .orderBy('date')
      .snapshots();

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      body: Container(
        padding: const EdgeInsets.all(20),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            StreamBuilder<QuerySnapshot>(
                stream: _collectionStream,
                builder: (context, collectionSnapshot) {
                  if (!collectionSnapshot.hasData) return const CircularProgressIndicator(color: Colors.black,);
                  return Expanded(
                      child: ListView.builder(
                          itemCount: collectionSnapshot.data!.size,
                          itemBuilder: (context, index) {

                            final Stream<DocumentSnapshot<Map<String, dynamic>>> cardsCount = FirebaseFirestore.instance
                                .collection('users')
                                .doc(FirebaseAuth.instance.currentUser!.uid)
                                .snapshots();

                            return StreamBuilder<DocumentSnapshot>(
                                stream: cardsCount,
                                builder: (context, snapshot) {
                                  if (!snapshot.hasData) return const CircularProgressIndicator(color: Colors.black,);
                                  return Column(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: [
                                      Text('${(collectionSnapshot.data?.docs[index].data() as Map<String, dynamic>)['name']}',
                                        style: const TextStyle(
                                          fontSize: 35,
                                          fontWeight: FontWeight.bold
                                        ),
                                      ),
                                      Row(
                                        children: [
                                          Image.asset('images/card.png', width: 25, color: Colors.black,),
                                          Text('${(snapshot.data?.data() as Map<String, dynamic>)['${(collectionSnapshot.data?.docs[index].data() as Map<String, dynamic>)['name']} classic']['count']} / ${(collectionSnapshot.data?.docs[index].data() as Map<String, dynamic>)['count']}',
                                            style: const TextStyle(
                                                fontSize: 25,
                                                fontWeight: FontWeight.normal
                                            ),
                                          ),
                                        ],
                                      ),
                                      Row(
                                        children: [
                                          Image.asset('images/shiny.png', width: 25, color: Colors.black,),
                                          Text('${(snapshot.data?.data() as Map<String, dynamic>)['${(collectionSnapshot.data?.docs[index].data() as Map<String, dynamic>)['name']} shiny']['count']} / ${(collectionSnapshot.data?.docs[index].data() as Map<String, dynamic>)['count']}',
                                            style: const TextStyle(
                                                fontSize: 25,
                                                fontWeight: FontWeight.normal
                                            ),
                                          ),
                                        ],
                                      ),
                                      const Divider(
                                        color: Colors.black,
                                        height: 50,
                                        thickness: 2,
                                        indent: 20,
                                        endIndent: 20,
                                      )
                                    ],
                                  );
                                });


                          })
                  );
                })
          ],
        ),
      ),
      bottomNavigationBar: myBottomBar(context, 1),
    );
  }

}