import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:wankuldex/presentation/ui/view_models/myBottomBar.dart';
import 'package:wankuldex/presentation/ui/views/connectionPage.dart';
import 'package:wankuldex/presentation/ui/views/deleteAccountPage.dart';

class SettingsPage extends StatefulWidget {
  const SettingsPage({super.key});

  @override
  State<SettingsPage> createState() => _SettingsPageState();
}

class _SettingsPageState extends State<SettingsPage> {

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            ElevatedButton.icon(   // <-- ElevatedButton
              onPressed: () async {
                await FirebaseAuth.instance.signOut()
                  .then((value) {
                      Navigator.pushReplacement(
                          context,
                          MaterialPageRoute(builder: (context) => const ConnectionPage())
                      );
                  });
              },
              style: ElevatedButton.styleFrom(backgroundColor: Colors.black),
              icon: const Icon(
                Icons.logout,
                size: 24.0,
              ),
              label: const Text('Déconnexion'),
            ),
            ElevatedButton.icon(   // <-- ElevatedButton
              onPressed: () {
                AwesomeDialog(
                  context: context,
                  dialogType: DialogType.warning,
                  animType: AnimType.rightSlide,
                  title: 'Suppression de compte',
                  desc: "Vous êtes sur le point de supprimer votre compte. Avez vous bien réfléchi à la question ?",
                  btnOkOnPress: () async {

                    String uid = FirebaseAuth.instance.currentUser!.uid;

                    await FirebaseAuth.instance.currentUser!.delete()
                        .then((value) {

                      FirebaseFirestore.instance
                          .collection('users')
                          .doc(uid)
                          .delete()
                          .then((value) => debugPrint('5555555555'))
                          .onError((error, stackTrace) => debugPrint('999999999 $error'));

                      Navigator.pushReplacement(
                          context,
                          MaterialPageRoute(builder: (context) => const ConnectionPage())
                      );
                    })
                    .catchError((e) {
                      e as FirebaseAuthException;
                      if (e.code == 'requires-recent-login') {
                        showCupertinoModalPopup(
                            context: context,
                            builder: (context) => const DeleteAccountPage()
                        );
                      }
                    });
                  },
                  btnCancelOnPress: () {}
                ).show();
              },
              style: ElevatedButton.styleFrom(backgroundColor: Colors.red),
              icon: const Icon(
                Icons.delete_forever,
                size: 24.0,
              ),
              label: Text('Supprimer mon compte'),
            )
          ],
        ),
      ),
      bottomNavigationBar: myBottomBar(context, 2),
    );
  }
}