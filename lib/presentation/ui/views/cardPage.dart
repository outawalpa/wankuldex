import 'dart:ui';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:wankuldex/data/models/playingCard.dart';

class CardPage extends StatefulWidget {

  final PlayingCard card;

  const CardPage({super.key, required this.card});

  @override
  State<CardPage> createState() => _CardPageState();
}

class _CardPageState extends State<CardPage> {

  Map<dynamic, dynamic> classicMap = {};
  Map<dynamic, dynamic> shinyMap = {};
  late final Stream<DocumentSnapshot> _cardStream;

  @override
  void initState() {
    super.initState();

    _cardStream = FirebaseFirestore.instance
        .collection('users')
        .doc(FirebaseAuth.instance.currentUser!.uid)
        .snapshots();
  }

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      appBar: AppBar(
        title: const Text("Wankuldex"),
      ),
      backgroundColor: Colors.transparent,
      body: BackdropFilter(
        filter: ImageFilter.blur(sigmaX: 10, sigmaY: 10),
        child: Center(
          child: Column(
            children: [
              Container(
                padding: const EdgeInsets.all(20),
                child: Image.network(widget.card.image),
              ),
              StreamBuilder<DocumentSnapshot>(
                  stream: _cardStream,
                  builder: (context, snapshot) {
                    if (!snapshot.hasData) {
                      return const CircularProgressIndicator(color: Colors.black,);
                    }

                    if (snapshot.hasData && !snapshot.data!.exists) {

                      createCardsMap();
                      return const CircularProgressIndicator(color: Colors.black,);
                    }


                    if (snapshot.data!['${widget.card.collection} classic']['${widget.card.id}'] == null) {
                      createCardClassic(snapshot.data!['${widget.card.collection} classic']);
                      return const CircularProgressIndicator(color: Colors.black,);
                    }

                    if(snapshot.data!['${widget.card.collection} shiny']['${widget.card.id}'] == null) {
                      createCardShiny(snapshot.data!['${widget.card.collection} shiny']);
                      return const CircularProgressIndicator(color: Colors.black,);
                    }

                    classicMap = snapshot.data!['${widget.card.collection} classic'];
                    shinyMap = snapshot.data!['${widget.card.collection} shiny'];

                    return Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            InkWell(
                              onTap: () async {
                                setState(() {
                                  if (classicMap['${widget.card.id}'] > 0) {

                                    classicMap['${widget.card.id}'] = classicMap['${widget.card.id}'] - 1;

                                    if (classicMap['${widget.card.id}'] == 0) {
                                      classicMap['count'] = classicMap['count'] - 1 ;
                                    }

                                    FirebaseFirestore.instance
                                        .collection('users')
                                        .doc(FirebaseAuth.instance.currentUser!.uid)
                                        .update({'${widget.card.collection} classic':classicMap})
                                        .then((value) => debugPrint('Update classic map for card ${widget.card.id}'))
                                        .catchError((error) => debugPrint("Failed to update classic card ${widget.card.id} : $error"));

                                  }
                                });
                              },
                              child: const Icon(Icons.navigate_before_rounded,
                                color: Colors.white,
                                size: 50,
                              ),
                            ),

                            Container(
                              height: 70,
                              width: 70,
                              alignment: Alignment.center,
                              decoration: const BoxDecoration(
                                  image: DecorationImage(
                                    image: AssetImage('images/card.png'),
                                  )

                              ),
                              child: Text(classicMap['${widget.card.id}'].toString(),
                                style: const TextStyle(
                                    fontSize: 40,
                                    color: Colors.white
                                ),
                              ),
                            ),
                            InkWell(
                              onTap: () async {
                                setState(() {
                                  classicMap['${widget.card.id}'] = classicMap['${widget.card.id}'] + 1;

                                  if (classicMap['${widget.card.id}'] == 1) {
                                    classicMap['count'] = classicMap['count'] + 1 ;
                                  }

                                  FirebaseFirestore.instance
                                      .collection('users')
                                      .doc(FirebaseAuth.instance.currentUser!.uid)
                                      .update({'${widget.card.collection} classic':classicMap})
                                      .then((value) => debugPrint('Update classic map for card ${widget.card.id}'))
                                      .catchError((error) => debugPrint("Failed to update classic card ${widget.card.id} : $error"));
                                });
                              },
                              child: const Icon(Icons.navigate_next_rounded,
                                color: Colors.white,
                                size: 50,
                              ),
                            )

                          ],
                        ),

                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            InkWell(
                              onTap: () async {
                                setState(() {
                                  if (shinyMap['${widget.card.id}'] > 0) {

                                    shinyMap['${widget.card.id}'] = shinyMap['${widget.card.id}'] - 1;

                                    if (shinyMap['${widget.card.id}'] == 0) {
                                      shinyMap['count'] = shinyMap['count'] - 1 ;
                                    }

                                    FirebaseFirestore.instance
                                        .collection('users')
                                        .doc(FirebaseAuth.instance.currentUser!.uid)
                                        .update({'${widget.card.collection} shiny':shinyMap})
                                        .then((value) => debugPrint('Update shiny map for card ${widget.card.id}'))
                                        .catchError((error) => debugPrint("Failed to update shiny card ${widget.card.id} : $error"));

                                  }
                                });
                              },
                              child: const Icon(Icons.navigate_before_rounded,
                                color: Colors.white,
                                size: 50,
                              ),
                            ),

                            Container(
                              height: 70,
                              width: 70,
                              alignment: Alignment.center,
                              decoration: const BoxDecoration(
                                  image: DecorationImage(
                                    image: AssetImage('images/shiny.png'),
                                  )

                              ),
                              child: Text(shinyMap['${widget.card.id}'].toString(),
                                style: const TextStyle(
                                    fontSize: 40,
                                    color: Colors.white
                                ),
                              ),
                            ),
                            InkWell(
                              onTap: () async {
                                setState(() {
                                  shinyMap['${widget.card.id}'] = shinyMap['${widget.card.id}'] + 1;

                                  if (shinyMap['${widget.card.id}'] == 1) {
                                    shinyMap['count'] = shinyMap['count'] + 1 ;
                                  }

                                  FirebaseFirestore.instance
                                      .collection('users')
                                      .doc(FirebaseAuth.instance.currentUser!.uid)
                                      .update({'${widget.card.collection} shiny':shinyMap})
                                      .then((value) => debugPrint('Update shiny map for card ${widget.card.id}'))
                                      .catchError((error) => debugPrint("Failed to update shiny card ${widget.card.id} : $error"));
                                });
                              },
                              child: const Icon(Icons.navigate_next_rounded,
                                color: Colors.white,
                                size: 50,
                              ),
                            )

                          ],
                        ),
                      ],
                    );
                  })

            ],
          ),
        ),
      ),
    );
  }

  createCardsMap() async {

    await FirebaseFirestore.instance
        .collection('users')
        .doc(FirebaseAuth.instance.currentUser!.uid)
        .set({
          '${widget.card.collection} classic':{'count':0},
          '${widget.card.collection} shiny':{'count':0}
        });

    setState(() {});
  }

  createCardClassic(Map<dynamic, dynamic> classicMap) async {

    classicMap['${widget.card.id}'] = 0;

    await FirebaseFirestore.instance
        .collection('users')
        .doc(FirebaseAuth.instance.currentUser!.uid)
        .update({'${widget.card.collection} classic':classicMap})
        .then((value) => debugPrint('Create classic map for card ${widget.card.id}'))
        .catchError((error) => debugPrint("Failed to create classic card ${widget.card.id} : $error"));

    setState(() {});
  }

  createCardShiny(Map<dynamic, dynamic> shinyMap) async {
    shinyMap['${widget.card.id}'] = 0;

    await FirebaseFirestore.instance
        .collection('users')
        .doc(FirebaseAuth.instance.currentUser!.uid)
        .update({'${widget.card.collection} shiny':shinyMap})
        .then((value) => debugPrint('Create shiny map for card ${widget.card.id}'))
        .catchError((error) => debugPrint("Failed to create shiny card ${widget.card.id} : $error"));

    setState(() {});
  }
}