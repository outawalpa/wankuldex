import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:wankuldex/presentation/ui/views/collectionPage.dart';

class ConnectionPage extends StatefulWidget {
  const ConnectionPage({super.key});

  @override
  State<ConnectionPage> createState() => _ConnectionPageState();
}

class _ConnectionPageState extends State<ConnectionPage> with TickerProviderStateMixin {

  late AnimationController controllerIndicator;

  final _formKey = GlobalKey<FormState>();

  final usernameController = TextEditingController();
  final passwordController = TextEditingController();

  bool connecting = false;

  @override
  void initState() {
    super.initState();

    // Initialize the spinner
    controllerIndicator = AnimationController(
      vsync: this,
      duration: const Duration(seconds: 2),
    )..addListener(() {
      setState(() {});
    });
    controllerIndicator.repeat(reverse: true);

    WidgetsBinding.instance
        .addPostFrameCallback((_) => checkActiveConnection());

  }

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      backgroundColor: Colors.white,
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(20),
          child: Center(
            child: Form(
              key: _formKey,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  Image.network('https://wankul.fr/cdn/shop/files/wankul.jpg?v=1669382210'),
                  const Text(
                    'Email',
                    style: TextStyle(
                      fontSize: 25,
                      fontWeight: FontWeight.bold
                    ),
                  ),
                  TextFormField(
                    cursorColor: Colors.black,
                    decoration: const InputDecoration(
                      fillColor: Colors.white,
                      filled: true,
                      border: OutlineInputBorder(
                          borderSide: BorderSide(
                            color: Colors.black,
                          )),
                      focusColor: Colors.white,
                      focusedBorder: OutlineInputBorder(
                          borderSide: BorderSide(
                            color: Colors.grey,
                          )),
                      hintText: 'Email',
                    ),
                    controller: usernameController,
                    keyboardType: TextInputType.emailAddress,
                    validator: (value) {
                      if (value == null || value.isEmpty) {
                        return 'Email requis';
                      }
                      return null;
                    },
                  ),
                  const SizedBox(height: 10),
                  const Text(
                    'Mot de passe',
                    style: TextStyle(
                        fontSize: 25,
                        fontWeight: FontWeight.bold
                    ),
                  ),
                  TextFormField(
                    cursorColor: Colors.black,
                    decoration: const InputDecoration(
                      fillColor: Colors.white,
                      filled: true,
                      border: OutlineInputBorder(
                          borderSide: BorderSide(
                            color: Colors.black,
                          )),
                      focusColor: Colors.white,
                      focusedBorder: OutlineInputBorder(
                          borderSide: BorderSide(
                            color: Colors.grey,
                          )),
                      hintText: 'Mot de passe',
                    ),
                    controller: passwordController,
                    keyboardType: TextInputType.visiblePassword,
                    obscureText: true,
                    validator: (value) {
                      if (value == null || value.isEmpty) {
                        return 'Mot de passe requis';
                      }
                      return null;
                    },
                  ),

                  const SizedBox(height: 30),

                  Visibility(
                      visible: connecting,
                      child: Container(
                        alignment: Alignment.center,
                        child: SizedBox(
                            width: 50,
                            height: 50,
                            child: CircularProgressIndicator(
                              value: controllerIndicator.value,
                              semanticsLabel: 'Linear progress indicator',
                              color: Colors.black,
                            )
                        ),
                      )
                  ),

                  Visibility(
                    visible: !connecting,
                    child: ElevatedButton(
                      style: ElevatedButton.styleFrom(backgroundColor: Colors.black),
                      child: const Text("CONNEXION"),
                      onPressed: () {
                        signIn(context);
                      },
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  checkActiveConnection() async {
    if (FirebaseAuth.instance.currentUser != null) {

      controllerIndicator.dispose();

      Navigator.pushReplacement(
          context,
          MaterialPageRoute(builder: (context) => const CollectionPage())
      );
    }
  }

  signIn(BuildContext context) async {
    if (_formKey.currentState!.validate()) {

      connecting = true;

      await FirebaseAuth.instance.signInWithEmailAndPassword(
          email: usernameController.text, 
          password: passwordController.text)
      .then((userCredential) async {

        controllerIndicator.dispose();

        Navigator.pushReplacement(
            context,
            MaterialPageRoute(builder: (context) => const CollectionPage())
        );

      })
      .catchError((e) {

        e as FirebaseAuthException;
        debugPrint('ERROR : $e');

        if (e.code == 'user-not-found') {
          signUp(context);
        } else {

          AwesomeDialog(
            context: context,
            dialogType: DialogType.error,
            animType: AnimType.rightSlide,
            title: 'Erreur',
            desc: "Une erreur s'est produite. code ${e.code} ",
            btnOkOnPress: () {},
          ).show();

          FirebaseAuth.instance.signOut();
          usernameController.clear();
          passwordController.clear();
          connecting = false;
        }

      });

    }
  }

  signUp(BuildContext context) async {
    if (_formKey.currentState!.validate()) {

      connecting = true;

      await FirebaseAuth.instance.createUserWithEmailAndPassword(
          email: usernameController.text,
          password: passwordController.text)
          .then((userCredential) async {

            controllerIndicator.dispose();

            Navigator.pushReplacement(
                context,
                MaterialPageRoute(builder: (context) => const CollectionPage())
            );

          })
          .catchError((e) {

        AwesomeDialog(
          context: context,
          dialogType: DialogType.error,
          animType: AnimType.rightSlide,
          title: 'Erreur',
          desc: "Une erreur s'est produite. code ${e.code} ",

          btnOkOnPress: () {},
        ).show();

        FirebaseAuth.instance.signOut();
        usernameController.clear();
        passwordController.clear();

        debugPrint('ERROR : $e');

        connecting = false;
      });

    }
  }
}