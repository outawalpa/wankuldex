import 'package:flutter/material.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/services.dart';
import 'package:wankuldex/presentation/ui/views/connectionPage.dart';
import 'package:wankuldex/presentation/ui/views/homePage.dart';

void main() async {

  WidgetsFlutterBinding.ensureInitialized();
  SystemChrome.setEnabledSystemUIMode(SystemUiMode.manual, overlays: []);

  await Firebase.initializeApp(
    //options: DefaultFirebaseOptions.currentPlatform,
  );

  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'WANKULDEX',
      theme: ThemeData(
        primaryColor: Colors.black,
        fontFamily: 'Dimbo'
      ),
      home: const HomePage(),
    );
  }
}